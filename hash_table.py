# hash table without conflict resolution
class HashTable:
    def __init__(self):
        self.MAX = 10
        self.arr = [None for i in range(self.MAX)]

    def get_hash(self, key):
        h = 0
        for char in key:
            h += ord(char)
        return h % self.MAX

    def __setitem__(self, key, value):
        h = self.get_hash(key)
        self.arr[h] = value

    def __getitem__(self, key):
        h = self.get_hash(key)
        return self.arr[h]

    def __delitem__(self, key):
        h = self.get_hash(key)
        self.arr[h] = None

    def show(self):
        print('---Hash Table---')
        for item in self.arr:
            print(item)
        print('----------------')


t = HashTable()

t['march 6'] = 120
t['march 7'] = 424
t['march 8'] = 14
t['march 9'] = 49

t.show()

print(t['march 9'])

del t['march 9']

print(t['march 9'])

t.show()
