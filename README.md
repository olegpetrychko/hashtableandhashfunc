# Hash Table and Hash Func



## Getting started

Download the repository archive using the site or using one of the commands:

```
git clone https://gitlab.com/olegpetrychko/hashtableandhashfunc.git
```
or
```
git clone git@gitlab.com:olegpetrychko/hashtableandhashfunc.git
```

### Files

#### 1) dictionary.py

This file describes how to create dictionaries and what values you can store in it.

#### 2) hash_table.py

This file describes a simple implementation of a hash table.\
Important: The problem of collision is not solved here.

#### 3) hash_table_chaining.py

This file implements a hash table that uses the chain method to resolve the collision

#### 4) list_vs_dict.py

List against dictionary
