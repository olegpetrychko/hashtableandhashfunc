# To work with the dictionary, you need to create it. This can be done in several ways.

print("First, using the literal:")
d = {}
print(d)
# {}

d = {'dict': 1, 'dictionary': 2}
print(d)
# {'dict': 1, 'dictionary': 2}

print("Second, using the dict function:")
d = dict(short='dict', long='dictionary')
print(d)
# {'short': 'dict', 'long': 'dictionary'}

d = dict([(1, 1), (2, 4)])
print(d)
# {1: 1, 2: 4}

print("Third, with the fromkeys method:")
d = dict.fromkeys(['a', 'b'])
print(d)
# {'a': None, 'b': None}

d = dict.fromkeys(['a', 'b'], 100)
print(d)
# {'a': 100, 'b': 100}

print("Fourth, by using dictionary generators:")
d = {a: a ** 2 for a in range(7)}
print(d)
# {0: 0, 1: 1, 2: 4, 3: 9, 4: 16, 5: 25, 6: 36}

print("Add entries to dictionary and extract key values:")
d = {1: 2, 2: 4, 3: 9}
print(d[1])
# 2

d[4] = 4 ** 2
print(d)
# {1: 2, 2: 4, 3: 9, 4: 16}
