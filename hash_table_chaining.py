# hash table with conflict resolution
class HashTable:
    def __init__(self):
        self.MAX = 10
        self.arr = [[] for i in range(self.MAX)]

    def get_hash(self, key):
        h = 0
        for char in key:
            h += ord(char)
        return h % self.MAX

    def __setitem__(self, key, value):
        h = self.get_hash(key)
        found = False
        for idx, element in enumerate(self.arr[h]):
            if len(element) == 2 and element[0] == key:
                self.arr[h][idx] = (key, value)
                found = True
                break
        if not found:
            self.arr[h].append((key, value))

    def __getitem__(self, key):
        h = self.get_hash(key)
        for element in self.arr[h]:
            if element[0] == key:
                return element[1]
        return None

    def __delitem__(self, key):
        h = self.get_hash(key)
        for idx, element in enumerate(self.arr[h]):
            if element[0] == key:
                del self.arr[h][idx]

    def show(self):
        print('---Hash Table---')
        for item in self.arr:
            print(item)
        print('----------------')


t = HashTable()

t['march 6'] = 120
t['march 7'] = 424
t['march 8'] = 14
t['march 9'] = 49
t['march 10'] = 523

t.show()

print(t['march 9'])
print(t['march 10'])

del t['march 9']

print(t['march 9'])
print(t['march 10'])

t.show()
